import os
import numpy as np
import matplotlib.pyplot as plt
from keras.models import load_model
from keras.preprocessing import image

#caricamento dei modelli
modelGender = load_model('./male/NetworkGender.h5')
modelBeard = load_model('./beard/NetworkBeard.h5')
modelBald = load_model('./bald/NetworkBald.h5')
modelYoung = load_model('./young/NetworkYoung.h5')
modelSmiling = load_model('./smiling/NetworkSmiling.h5')
modelLipstick = load_model('./wearing_lipstick/NetworkLipstick.h5')

#immagine di test
img_path = 'D:/CelebA/ImmaginiDatestare/000038.jpg'




img = image.load_img(img_path, target_size=(150, 150))
img_tensor = image.img_to_array(img)
img_tensor = np.expand_dims(img_tensor, axis=0)

img_tensor /= 255.


name = os.path.basename(img_path)

plt.imshow(img_tensor[0])
plt.show()

results=[]#array dei risultati

results.append(name)

predictionMale = modelGender.predict(img_tensor)
predictionBeard = modelBeard.predict(img_tensor)
predictionBald = modelBald.predict(img_tensor)
predictionYoung = modelYoung.predict(img_tensor)
predictionSmiling = modelSmiling.predict(img_tensor)
predictionLipsting = modelLipstick.predict(img_tensor)

print(predictionMale)
print(predictionBeard)
print(predictionBald)
print(predictionYoung)
print(predictionSmiling)
print(predictionLipsting)

if(predictionMale >= 0.5):
    print('male')
    results.append(1)#uomo


else:
    print('female')
    results.append(0)#donna

if (predictionBeard >= 0.5):
    print('beard')
    results.append(1) #barba
else:
    print('not beard')
    results.append(0) #non barba

if (predictionBald >= 0.5):
    print('bald')
    results.append(1)#calvo
else:
    print('not bald')
    results.append(0)#non calvo

if (predictionBald >= 0.5):
    print('young')
    results.append(1)#giovane
else:
    print('old')
    results.append(0)#vecchio

if (predictionSmiling >= 0.5):
    print('smile')
    results.append(1)#sorride
else:
    print('not smile')
    results.append(0)#non sorride

if (predictionLipsting >= 0.5):
    print('lipstick')
    results.append(1)#rossetto
else:
    print('not lipstick')
    results.append(0)#non rossetto
print(results)

#salvataggio in un file csv

#nameCSV = [name]
#male = [results[1]]
#beard = [results[2]]

# dictionary of lists
#dict = {'name': nameCSV, 'Male': male, 'Beard': beard}

#df = pd.DataFrame(dict)

# saving the dataframe
#df.to_csv('file1.csv')






