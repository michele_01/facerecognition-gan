import os, shutil
import pandas as pd

original_dataset_dir = 'D:/CelebA/CelebA'
base_dir = 'D:/CelebA/CelebA_small_Gender'
os.mkdir(base_dir)


#creazione delle directory

train_dir = os.path.join(base_dir, 'train')
os.mkdir(train_dir)

validation_dir = os.path.join(base_dir, 'validation')
os.mkdir(validation_dir)

test_dir = os.path.join(base_dir, 'test')
os.mkdir(test_dir)

#creazione sottocartelle

train_male = os.path.join(train_dir, 'male')
os.mkdir(train_male)

train_female = os.path.join(train_dir, 'female')
os.mkdir(train_female)

validation_male = os.path.join(validation_dir, 'male')
os.mkdir(validation_male)

validation_female = os.path.join(validation_dir, 'female')
os.mkdir(validation_female)

test_male = os.path.join(test_dir, 'male')
os.mkdir(test_male)

test_female = os.path.join(test_dir, 'female')
os.mkdir(test_female)

# MALE

# train directory
csv_male_train = pd.read_csv(
    './data/maleTrain.csv',
    index_col=None)

fnames = list(csv_male_train['image_id'].values)

for name in fnames:
    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_male, name)
    shutil.copyfile(src, dst)

#validation directory
csv_male_validation = pd.read_csv('./data/maleValidation.csv', index_col=None)

fnames = list(csv_male_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_male, name)
    shutil.copyfile(src, dst)


#test directory
csv_male_test = pd.read_csv('./data/maleTest.csv', index_col=None)


fnames = list(csv_male_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_male, name)
    shutil.copyfile(src, dst)


#FEMALE

#train directory
csv_female_train = pd.read_csv('./data/femaleTrain.csv', index_col=None)


fnames = list(csv_female_train['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_female, name)
    shutil.copyfile(src, dst)

#validation directory
csv_female_validation = pd.read_csv('./data/femaleValidation.csv', index_col=None)


fnames = list(csv_female_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_female, name)
    shutil.copyfile(src, dst)

#test directory
csv_female_test = pd.read_csv('./data/femaleTest.csv', index_col=None)


fnames = list(csv_female_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_female, name)
    shutil.copyfile(src, dst)