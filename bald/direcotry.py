import os, shutil
import pandas as pd

original_dataset_dir = 'D:/CelebA/CelebA'
base_dir = 'D:/CelebA/CelebA_small_Bald'
os.mkdir(base_dir)


#creazione delle directory

train_dir = os.path.join(base_dir, 'train')
os.mkdir(train_dir)

validation_dir = os.path.join(base_dir, 'validation')
os.mkdir(validation_dir)

test_dir = os.path.join(base_dir, 'test')
os.mkdir(test_dir)

#creazione sottocartelle

train_bald = os.path.join(train_dir, 'bald')
os.mkdir(train_bald)

train_notbald = os.path.join(train_dir, 'NotBald')
os.mkdir(train_notbald)

validation_bald = os.path.join(validation_dir, 'bald')
os.mkdir(validation_bald)

validation_notbald = os.path.join(validation_dir, 'NotBald')
os.mkdir(validation_notbald)

test_bald = os.path.join(test_dir, 'bald')
os.mkdir(test_bald)

test_notbald = os.path.join(test_dir, 'NotBald')
os.mkdir(test_notbald)


# train directory
csv_male_train = pd.read_csv(
    './bald/baldTrain.csv',
    index_col=None)

fnames = list(csv_male_train['image_id'].values)

for name in fnames:
    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_bald, name)
    shutil.copyfile(src, dst)

#validation directory
csv_male_validation = pd.read_csv('./bald/baldValidation.csv', index_col=None)

fnames = list(csv_male_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_bald, name)
    shutil.copyfile(src, dst)


#test directory
csv_male_test = pd.read_csv('./bald/baldTest.csv', index_col=None)


fnames = list(csv_male_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_bald, name)
    shutil.copyfile(src, dst)




#train directory
csv_notbald_train = pd.read_csv('./bald/baldNotTrain.csv', index_col=None)


fnames = list(csv_notbald_train['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_notbald, name)
    shutil.copyfile(src, dst)

#validation directory
csv_notbald_validation = pd.read_csv('./bald/baldNotValidation.csv', index_col=None)


fnames = list(csv_notbald_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_notbald, name)
    shutil.copyfile(src, dst)

#test directory
csv_notbald_test = pd.read_csv('./bald/baldNotTest.csv', index_col=None)


fnames = list(csv_notbald_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_notbald, name)
    shutil.copyfile(src, dst)