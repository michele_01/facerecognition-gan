import numpy as np
import matplotlib.pyplot as plt

from keras.models import load_model
from keras.preprocessing import image



model = load_model('NetworkBald.h5')


img_path = 'D:/CelebA/ImmaginiDaTestare/001383.jpg'
img = image.load_img(img_path, target_size=(150,150))


img = image.load_img(img_path, target_size=(150, 150))
img_tensor = image.img_to_array(img)
img_tensor = np.expand_dims(img_tensor, axis=0)
# Remember that the model was trained on inputs
# that were preprocessed in the following way:
img_tensor /= 255.
# Its shape is (1, 150, 150, 3)
#print(img_tensor.shape)

plt.imshow(img_tensor[0])
plt.show()

prediction = model.predict(img_tensor)
print(prediction)

results=[]

if(prediction >= 0.5):
    results.append(1)#calvo

else:
    results.append(0)#no calvo

print(results)








