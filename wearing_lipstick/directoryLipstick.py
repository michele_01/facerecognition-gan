import os, shutil
import pandas as pd

original_dataset_dir = 'D:/CelebA/CelebA'
base_dir = 'D:/CelebA/CelebA_small_Lipstick'
os.mkdir(base_dir)


#creazione delle directory

train_dir = os.path.join(base_dir, 'train')
os.mkdir(train_dir)

validation_dir = os.path.join(base_dir, 'validation')
os.mkdir(validation_dir)

test_dir = os.path.join(base_dir, 'test')
os.mkdir(test_dir)

#creazione sottocartelle

train_lipstick = os.path.join(train_dir, 'lipstick')
os.mkdir(train_lipstick)

train_Notlipstick = os.path.join(train_dir, 'NotLipstick')
os.mkdir(train_Notlipstick)

validation_lipstick = os.path.join(validation_dir, 'lipstick')
os.mkdir(validation_lipstick)

validation_Notlipstick = os.path.join(validation_dir, 'NotLipstick')
os.mkdir(validation_Notlipstick)

test_lipstick = os.path.join(test_dir, 'lipstick')
os.mkdir(test_lipstick)

test_Notlipstick = os.path.join(test_dir, 'NotLipstick')
os.mkdir(test_Notlipstick)



# train directory
csv_lipstick_train = pd.read_csv(
    './data/lipstickTrain.csv',
    index_col=None)

fnames = list(csv_lipstick_train['image_id'].values)

for name in fnames:
    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_lipstick, name)
    shutil.copyfile(src, dst)

#validation directory
csv_lipstick_validation = pd.read_csv('./data/lipstickValidation.csv', index_col=None)

fnames = list(csv_lipstick_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_lipstick, name)
    shutil.copyfile(src, dst)


#test directory
csv_lipstick_test = pd.read_csv('./data/lipstickTest.csv', index_col=None)


fnames = list(csv_lipstick_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_lipstick, name)
    shutil.copyfile(src, dst)



#train directory
csv_NotLipstick_train = pd.read_csv('./data/lipstickNotTrain.csv', index_col=None)


fnames = list(csv_NotLipstick_train['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_Notlipstick, name)
    shutil.copyfile(src, dst)

#validation directory
csv_NotLipstick_validation = pd.read_csv('./data/lipstickNotValidation.csv', index_col=None)


fnames = list(csv_NotLipstick_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_Notlipstick, name)
    shutil.copyfile(src, dst)

#test directory
csv_NotLipstick_test = pd.read_csv('./data/lipstickNotTest.csv', index_col=None)


fnames = list(csv_NotLipstick_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_Notlipstick, name)
    shutil.copyfile(src, dst)