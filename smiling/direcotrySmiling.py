import os, shutil
import pandas as pd

original_dataset_dir = 'D:/CelebA/CelebA'
base_dir = 'D:/CelebA/CelebA_small_Smiling'
os.mkdir(base_dir)


#creazione delle directory

train_dir = os.path.join(base_dir, 'train')
os.mkdir(train_dir)

validation_dir = os.path.join(base_dir, 'validation')
os.mkdir(validation_dir)

test_dir = os.path.join(base_dir, 'test')
os.mkdir(test_dir)

#creazione sottocartelle

train_smiling = os.path.join(train_dir, 'smiling')
os.mkdir(train_smiling)

train_notSmiling = os.path.join(train_dir, 'NotSmiling')
os.mkdir(train_notSmiling)

validation_smiling = os.path.join(validation_dir, 'smiling')
os.mkdir(validation_smiling)

validation_notSmiling = os.path.join(validation_dir, 'NotSmiling')
os.mkdir(validation_notSmiling)

test_smiling = os.path.join(test_dir, 'smiling')
os.mkdir(test_smiling)

test_notSmiling = os.path.join(test_dir, 'NotSmiling')
os.mkdir(test_notSmiling)

# MALE

# train directory
csv_male_train = pd.read_csv(
    './data/SmilingTrain.csv',
    index_col=None)

fnames = list(csv_male_train['image_id'].values)

for name in fnames:
    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_smiling, name)
    shutil.copyfile(src, dst)

#validation directory
csv_male_validation = pd.read_csv('./data/SmilingValidation.csv', index_col=None)

fnames = list(csv_male_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_smiling, name)
    shutil.copyfile(src, dst)


#test directory
csv_male_test = pd.read_csv('./data/SmilingTest.csv', index_col=None)


fnames = list(csv_male_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_smiling, name)
    shutil.copyfile(src, dst)


#FEMALE

#train directory
csv_female_train = pd.read_csv('./data/NotSmilingTrain.csv', index_col=None)


fnames = list(csv_female_train['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_notSmiling, name)
    shutil.copyfile(src, dst)

#validation directory
csv_female_validation = pd.read_csv('./data/NotSmilingValidation.csv', index_col=None)


fnames = list(csv_female_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_notSmiling, name)
    shutil.copyfile(src, dst)

#test directory
csv_female_test = pd.read_csv('./data/NotSmilingTest.csv', index_col=None)


fnames = list(csv_female_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_notSmiling, name)
    shutil.copyfile(src, dst)