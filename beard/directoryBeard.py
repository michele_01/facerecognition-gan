import os, shutil
import pandas as pd

original_dataset_dir = 'D:/CelebA/CelebA'
base_dir = 'D:/CelebA/CelebA_small_Beard'
os.mkdir(base_dir)


#creazione delle directory

train_dir = os.path.join(base_dir, 'train')
os.mkdir(train_dir)

validation_dir = os.path.join(base_dir, 'validation')
os.mkdir(validation_dir)

test_dir = os.path.join(base_dir, 'test')
os.mkdir(test_dir)

#creazione sottocartelle

train_beard = os.path.join(train_dir, 'beard')
os.mkdir(train_beard)

train_NotBeard = os.path.join(train_dir, 'NotBeard')
os.mkdir(train_NotBeard)

validation_beard = os.path.join(validation_dir, 'beard')
os.mkdir(validation_beard)

validation_NotBeard = os.path.join(validation_dir, 'NotBeard')
os.mkdir(validation_NotBeard)

test_beard = os.path.join(test_dir, 'beard')
os.mkdir(test_beard)

test_NotBeard = os.path.join(test_dir, 'NotBeard')
os.mkdir(test_NotBeard)

# MALE

# train directory
csv_male_train = pd.read_csv(
    './beardTrain.csv',
    index_col=None)

fnames = list(csv_male_train['image_id'].values)

for name in fnames:
    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_beard, name)
    shutil.copyfile(src, dst)

#validation directory
csv_male_validation = pd.read_csv('./beardValidation.csv', index_col=None)

fnames = list(csv_male_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_beard, name)
    shutil.copyfile(src, dst)


#test directory
csv_beard_test = pd.read_csv('./beardTest.csv', index_col=None)


fnames = list(csv_beard_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_beard, name)
    shutil.copyfile(src, dst)


#FEMALE

#train directory
csv_NotBeard_train = pd.read_csv('./beardNotTrain.csv', index_col=None)


fnames = list(csv_NotBeard_train['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(train_NotBeard, name)
    shutil.copyfile(src, dst)

#validation directory
csv_NotBeard_validation = pd.read_csv('./beardNotValidation.csv', index_col=None)


fnames = list(csv_NotBeard_validation['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(validation_NotBeard, name)
    shutil.copyfile(src, dst)

#test directory
csv_NotBeard_test = pd.read_csv('./beardNotTest.csv', index_col=None)


fnames = list(csv_NotBeard_test['image_id'].values)

for name in fnames:

    src = os.path.join(original_dataset_dir, name)
    dst = os.path.join(test_NotBeard, name)
    shutil.copyfile(src, dst)